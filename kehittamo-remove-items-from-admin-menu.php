<?php
/*
Plugin Name: Kehittämö Remove specific menus from admin
Plugin URI: http://www.kehittamo.fi
Description: Removing menu items from admin page and if user tries to go specific page redirect to admin home
Version: 0.1.1
Author: Kehittämö Oy / Jani Krunniniva
Author Email: asiakaspalvelu@kehittamo.fi
License: GPL2

  Copyright 2016 Kehittämö Oy (asiakaspalvelu@kehittamo.fi)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
namespace Kehittamo\Plugins\RemoveMenusFromAdmin;

define( 'Kehittamo\Plugins\RemoveMenusFromAdmin\PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'Kehittamo\Plugins\RemoveMenusFromAdmin\PLUGIN_URL', plugin_dir_url( __FILE__ ) );

class Load {

  /**
   * Construct the plugin
   */
  function __construct() {
    add_action( 'admin_menu', array( $this, 'custom_menu_page_removing' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'remove_customize_links' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'remove_customize_links') );
  }

  function remove_customize_links() {
    echo "<style>
      .hide-if-no-customize {display:none !important;}
      #wp-admin-bar-customize {display:none !important;}
    </style>";
  }

  /**
   * Removing menu items from admin page and if user tries to go specific page redirect to admin home
   */
  function custom_menu_page_removing() {

    //LIST OF ACCEPTING EMAILS
    $accepting_users    = [ 'asiakaspalvelu@kehittamo.fi' ];
    $banned_url         = [ 'themes.php', 'plugins.php' ];
    $current_user       = wp_get_current_user();
    $current_user_email = $current_user->user_email;

    $basename    = parse_url( basename( $_SERVER['REQUEST_URI'] ) );
    $currentpath = $basename['path'];
    if ( isset( $basename['query'] ) ) {
      $currentpath .= '?' . $basename['query'];
    }

    // check permissions : is super admin
    if ( is_super_admin() ) {
      return;
    }

    // check is current user in accepting list
    if ( in_array( $current_user_email, $accepting_users ) ) {
      return;
    }

    // remove menus
    remove_submenu_page( 'themes.php', 'themes.php' );
    remove_menu_page( 'plugins.php' );

    // check is current path in banned list array
    if ( in_array( $currentpath, $banned_url ) ) {
      wp_redirect( admin_url() );
    }

  }
}

$kehittamo_remove_menus_from_admin = new \Kehittamo\Plugins\RemoveMenusFromAdmin\Load();
